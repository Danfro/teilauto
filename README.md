# teilAuto

[![OpenStore](https://open-store.io/badges/en_US.png)](https://open-store.io/app/teilauto.danfro)

Unofficial webapp for the mobile page of a major German car sharing provider.
It does take a while to load - please be patient.

Nichtoffizielle webapp für die mobile Seite eines großen deutschen Carsharinganbieters.
Die Seite braucht eine Weile zum laden - hab etwas Geduld.

teilauto webapp link:
https://m.teilauto.net/webapp/suche.html

### Screenshot:

<img src="screenshot_readme.png" alt="screenshot" width="182"/>
